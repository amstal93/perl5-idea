Reparsing block
----------
Full reparse
----------
After typing
----------
sub something{
  s{test
    say 'hi';
    say 'hi'
  }{replace# comment }<caret>ment $3};
}

----------
Psi structure
----------
Perl5
  PsiPerlSubDefinitionImpl(SUB_DEFINITION)@main::something
    PsiElement(Perl5: sub)('sub')
    PsiWhiteSpace(' ')
    PerlSubNameElementImpl(Perl5: subname)('something')
    PsiPerlBlockImpl(Perl5: BLOCK)
      PsiElement(Perl5: {)('{')
      PsiWhiteSpace('\n  ')
      PsiPerlStatementImpl(Perl5: STATEMENT)
        PsiPerlReplacementRegexImpl(Perl5: REPLACEMENT_REGEX)
          PsiElement(Perl5: s)('s')
          PsiElement(Perl5: r{)('{')
          PsiPerlPerlRegexImpl(Perl5: PERL_REGEX)
            PsiElement(Perl5: regex)('test\n    say 'hi';\n    say 'hi'\n  ')
          PsiElement(Perl5: r})('}')
          PsiElement(Perl5: re{)('{')
          PsiPerlBlockBracelessImpl(Perl5: BLOCK_BRACELESS)
            PsiPerlStatementImpl(Perl5: STATEMENT)
              PsiPerlSubCallImpl(SUB_CALL)
                PsiPerlMethodImpl(Perl5: METHOD)
                  PerlSubNameElementImpl(Perl5: subname)('replace')
            PsiComment(Perl5: COMMENT_LINE)('# comment ')
          PsiElement(Perl5: r})('}')
          PsiPerlPerlRegexModifiersImpl(Perl5: PERL_REGEX_MODIFIERS)
            PsiElement(Perl5: /m)('m')
            PsiElement(Perl5: /m)('e')
            PsiElement(Perl5: /m)('n')
        PsiErrorElement:Semicolon expected
          <empty list>
      PsiPerlStatementImpl(Perl5: STATEMENT)
        PsiPerlSubCallImpl(SUB_CALL)
          PsiPerlMethodImpl(Perl5: METHOD)
            PerlSubNameElementImpl(Perl5: subname)('t')
          PsiWhiteSpace(' ')
          PsiPerlCallArgumentsImpl(Perl5: CALL_ARGUMENTS)
            PsiPerlScalarVariableImpl(Perl5: SCALAR_VARIABLE)
              PsiElement(Perl5: $$)('$')
              PerlVariableNameElementImpl(Perl5: SCALAR_NAME)('3')
      PsiElement(Perl5: })('}')
  PsiElement(Perl5: ;)(';')
  PsiWhiteSpace('\n')
  PsiErrorElement:<func definition>, <method definition>, <statement>, <sub definition>, Perl5: #@abstract, Perl5: #@deprecated, Perl5: #@inject, Perl5: #@method, Perl5: #@noinspection, Perl5: #@override, Perl5: #@returns, Perl5: #@type, Perl5: #@unknown, Perl5: BLOCK_NAME, Perl5: POD, Perl5: TryCatch::, Perl5: __DATA__, Perl5: __END__, Perl5: case, Perl5: default, Perl5: for, Perl5: foreach, Perl5: format, Perl5: fp_after, Perl5: fp_around, Perl5: fp_augment, Perl5: fp_before, Perl5: given, Perl5: if, Perl5: nyi, Perl5: package, Perl5: switch, Perl5: unless, Perl5: until, Perl5: when, Perl5: while or Perl5: { expected, got '}'
    PsiElement(Perl5: })('}')
  PsiWhiteSpace('\n')
